#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include "secrets.h"

#define LED_PIN D9 // Define the GPIO pin number
#define RESET_PIN 22 // Define the GPIO pin number

// Create an instance of the server
AsyncWebServer server(80);

void setup() {
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
  pinMode(RESET_PIN, OUTPUT);
  digitalWrite(RESET_PIN, HIGH);

  Serial.begin(9600);

  // Connect to Wi-Fi
  WiFi.begin(SECRET_SSID, REBOOT_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println(WiFi.localIP());

  server.on("/reboot_server", HTTP_POST, [](AsyncWebServerRequest *request){
    Serial.println("reset server");
    
    if(request->hasParam("password", true)) {
    AsyncWebParameter* p = request->getParam("password", true);
    if(p->value() == REBOOT_PASSWORD) {  // replace "your_password" with your actual password
      digitalWrite(LED_PIN, LOW); // Drive the pin LOW
      digitalWrite(LED_PIN, LOW);
      digitalWrite(RESET_PIN, LOW);
      delay(1000);
      digitalWrite(LED_PIN, HIGH);
      digitalWrite(RESET_PIN, HIGH);
      request->send(200);  // Return a successful HTTP response
    }
    else {
      request->send(403, "text/plain", "Wrong password"); // Forbidden
    }
  }
  else {
    request->send(400, "text/plain", "No password provided"); // Bad Request
  }
    
  });

  // Start the server
  server.begin();
}

void loop() {
  Serial.println(WiFi.localIP());
  delay(5000);
}
